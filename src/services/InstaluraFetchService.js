import { AsyncStorage } from 'react-native';

export default class InstaluraFetchService {

    static get(recurso) {

        const uri = `https://instalura-api.herokuapp.com/api${recurso}`;

        return AsyncStorage.getItem('usuario')
        .then(usuario => JSON.parse(usuario))
        .then(usuario => {
            const request = {
                headers: new Headers ({
                "X-AUTH-TOKEN": usuario.token
            })
            }
            return request
        })

        .then(request => fetch(uri,request))
        .then(resposta => resposta.json())
        .then(json => this.setState({fotos: json}))
        
        fetch('https://instalura-api.herokuapp.com/api/public/fotos/rafael')
        .then(response => response.json())

    }

}