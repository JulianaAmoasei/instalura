import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  FlatList,
  AsyncStorage,
} from 'react-native';

import Post from './Post';
import InstaluraFetchService from '../services/InstaluraFetchService';

export default class Feed extends Component {
  
  constructor() {
    super()
    this.state = {
      fotos: []
    }
  }

  componentDidMount() {
      InstaluraFetchService.get('/fotos')
      .then(json => this.setState({fotos: json}))
  }

  like = (idFoto) => {  
    const foto = this.state.fotos;

    AsyncStorage.getItem('usuario')
      .then(usuarioLogado => {

        let novaLista = []
        if(!foto.likeada) {
          novaLista = [
            ...foto.likers,
            {login: usuarioLogado}
          ]
        } else {
          novaLista = foto.likers.filter(liker => {
            return liker.login !== usuarioLogado
          });
        }

        return novaLista;
      })
      .then(novaLista => {
        const fotoAtualizada ={
          ...foto,
          likeada: !foto.likeada,
          likers: novaLista
        };

        this.fatualizaFotos(fotoAtualizada);
      });

    const uri = `https://instalura-api.herokuapp.com/api/fotos/${idFoto}/like`;
    AsyncStorage.getItem('usuario')
      .then(usuario => JSON.parse(usuario))
      .then(usuario => {
        const request = {
          method: 'POST',
          headers: new Headers({
            "X-AUTH-TOKEN": usuarion.token
          })
        }
      })
      .then(requestInfo => fetch(uri, requestInfo));
    
    let novaLista = [];
    if(!foto.likeada) {
      novaLista = [
        ...foto.likers,
        {login: 'meuUsuario'}
      ]
    } else {
      novaLista = foto.likers
        .filter(liker => liker.login != 'meuUsuario')
    }
    
    const fotoAtualizada = {
      ...foto,
      likeada: !foto.likeada,
      likers: novaLista
    }

    const fotos = this.state.fotos.map(foto => 
      foto.id === fotoAtualizada.id ? fotoAtualizada : foto)
    this.setState({fotos});

    }

    InstaluraFetchService.fo
  

  adicionaComentario = (idFoto, valorComentario, inputComentario) => {
    if(valorComentario === '')
      return;

    const foto = this.buscaPorId(idFoto);
    const uri = `https://instalura-api.herokuapp.com/api/${idFoto}`;

    AsyncStorage.getItem('token')
      .then(token => {
        return {
          method: 'POST',
          body: JSON.stringify({
            texto: valorComentario
          }),
          headers: new Headers({
            "Content-type": "application/json",
            "X-AUTH-TOKEN": token
          })
        };
      })

      .then(requestInfo => fetch(uri, requestInfo))
      .then(resposta => resposta.json())
      .then(comentario => [...foto.comentarios, comentario])
      .then(novaLista => {
        const fotoAtualizada = {
          ...foto,
          comentarios: novaLista
        }

        this.atualizaFotos(fotoAtualizada);
        inputComentario.clear();
      });

    const novaLista = [...foto.comentarios, {
      id: Math.random(),
      login: 'meuUsuario',
      texto: valorComentario
    }]
    
    const fotoAtualizada = {
      ...foto,
      comentarios: novaLista
    }

    const fotos = this.state.fotos.map(foto => 
      foto.id === fotoAtualizada.id ? fotoAtualizada : foto)
    this.setState({fotos})
  }
  
  render() {
    return (
      <FlatList
          data={this.state.fotos}
          keyExtractor={ item => item.id }
          renderItem={ ({item}) =>
              <Post foto={item}
                  likeCallback={this.like}
                  comentarioCallback={this.adicionaComentario}/>
          } />
    );
  }
}
